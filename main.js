// var h1_element = document.getElementById('header');
// console.log(h1_element);

var nav_links = document.getElementsByClassName('nav-link');
var dropdowns = document.getElementsByClassName('dropdown');

function toggleDropdown(){
    // for(i=0; i < dropdowns.length; i++){
    //     dropdowns[i].classList.toggle('condensed');
    // }
    this.children[1].classList.toggle('condensed')
}

for(i=0; i<nav_links.length; i++){
    nav_links[i].addEventListener('mouseenter', toggleDropdown);
    nav_links[i].addEventListener('mouseleave', toggleDropdown);
}